# Add project specific ProGuard rules here.
# By default, the flags in this file are appended to flags specified
# in /Users/sitaram/Library/Android/sdk/tools/proguard/proguard-android.txt
# You can edit the include path and order by changing the proguardFiles
# directive in build.gradle.
#
# For more details, see
#   http://developer.android.com/guide/developing/tools/proguard.html

# Add any project specific keep options here:

# If your project uses WebView with JS, uncomment the following
# and specify the fully qualified class name to the JavaScript interface
# class:
#-keepclassmembers class fqcn.of.javascript.interface.for.webview {
#   public *;
#}


-keep public class com.niki.config.**

-keep public class com.niki.config.**{
  public protected *;
}

-keepclasseswithmembernames class * {
    native <methods>;
}
 -keep class **.R$* {
 <fields>;
}
-dontwarn com.squareup.picasso.**
-dontwarn retrofit2.**
-dontwarn okio.**
-dontwarn com.zendesk.util.**

-dontwarn com.fasterxml.jackson.databind.**
-dontwarn com.pubnub.**

-keep class retrofit2.** { *; }
-keepattributes Signature
-keepattributes Exceptions

-keepclassmembers class * extends org.greenrobot.greendao.AbstractDao {
public static java.lang.String TABLENAME;
}
-keep class **$Properties

# If you do not use SQLCipher:
-dontwarn org.greenrobot.greendao.database.**
# If you do not use Rx:
-dontwarn rx.**

-keep class org.greenrobot.greendao.**

-keep class android.support.v7.internal.** { *; }
-keep interface android.support.v7.internal.** { *; }
-keep class android.support.v7.** { *; }
-keep interface android.support.v7.** { *; }


# For using GSON @Expose annotation
-keepattributes *Annotation*

# Gson specific classes
-keep class sun.misc.Unsafe { *; }
#-keep class com.google.gson.stream.** { *; }

# Application classes that will be serialized/deserialized over Gson
-keep class com.niki.models.** { *; }

-keep class com.google.gson.**
-keep class * implements com.google.gson.TypeAdapterFactory
-keep class * implements com.google.gson.JsonSerializer
-keep class * implements com.google.gson.JsonDeserializer
